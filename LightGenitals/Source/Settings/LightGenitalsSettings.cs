﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace LightGenitals
{
    public class SettingsController_LightGenitals : Mod
    {
        public SettingsController_LightGenitals(ModContentPack content) : base(content)
        {
            GetSettings<Settings_LightGenitals>();
        }

        public override string SettingsCategory()
        {
            return "lightGenitals_settingsName".Translate();
        }

        public override void DoSettingsWindowContents(Rect inRect)
        {
            Settings_LightGenitals.DoSettingsWindowContents(inRect);
        }
    }

    public class Settings_LightGenitals : ModSettings
    {
        public static float maleChanceToSpawnWithBreasts = 0f;
        public static float maleChanceToSpawnWithVagina = 0f;
        public static float maleChanceToSpawnWithPenis = 1f;
        public static float femaleChanceToSpawnWithBreasts = 1f;
        public static float femaleChanceToSpawnWithVagina = 1f;
        public static float femaleChanceToSpawnWithPenis = 0f;

        public static void DoSettingsWindowContents(Rect inRect)
        {
            Listing_Standard list = new Listing_Standard()
            {
                ColumnWidth = inRect.width
            };
            list.Begin(inRect);
            maleChanceToSpawnWithBreasts = list.DoLabelledSlider(maleChanceToSpawnWithBreasts, 0f, 1f, "maleChanceToSpawnWithBreasts".Translate(), null, UIUtility.percentagePresenter);
            maleChanceToSpawnWithVagina = list.DoLabelledSlider(maleChanceToSpawnWithVagina, 0f, 1f, "maleChanceToSpawnWithVagina".Translate(), null, UIUtility.percentagePresenter);
            maleChanceToSpawnWithPenis = list.DoLabelledSlider(maleChanceToSpawnWithPenis, 0f, 1f, "maleChanceToSpawnWithPenis".Translate(), null, UIUtility.percentagePresenter);
            femaleChanceToSpawnWithBreasts = list.DoLabelledSlider(femaleChanceToSpawnWithBreasts, 0f, 1f, "femaleChanceToSpawnWithBreasts".Translate(), null, UIUtility.percentagePresenter);
            femaleChanceToSpawnWithVagina = list.DoLabelledSlider(femaleChanceToSpawnWithVagina, 0f, 1f, "femaleChanceToSpawnWithVagina".Translate(), null, UIUtility.percentagePresenter);
            femaleChanceToSpawnWithPenis = list.DoLabelledSlider(femaleChanceToSpawnWithPenis, 0f, 1f, "femaleChanceToSpawnWithPenis".Translate(), null, UIUtility.percentagePresenter);
            list.End();
        }
        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref maleChanceToSpawnWithBreasts, "maleChanceToSpawnWithBreasts", maleChanceToSpawnWithBreasts, true);
            Scribe_Values.Look(ref maleChanceToSpawnWithVagina, "maleChanceToSpawnWithVagina", maleChanceToSpawnWithVagina, true);
            Scribe_Values.Look(ref maleChanceToSpawnWithPenis, "maleChanceToSpawnWithPenis", maleChanceToSpawnWithPenis, true);
            Scribe_Values.Look(ref femaleChanceToSpawnWithBreasts, "femaleChanceToSpawnWithBreasts", femaleChanceToSpawnWithBreasts, true);
            Scribe_Values.Look(ref femaleChanceToSpawnWithVagina, "femaleChanceToSpawnWithVagina", femaleChanceToSpawnWithVagina, true);
            Scribe_Values.Look(ref femaleChanceToSpawnWithPenis, "femaleChanceToSpawnWithPenis", femaleChanceToSpawnWithPenis, true);
        }
    }
}
