﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RimWorld;

namespace RimVore2
{
    public class RV2Component : GameComponent
    {
        private Dictionary<int, PawnData> pawnData = new Dictionary<int, PawnData>();
        public RuntimeUniqueIDsManager RuntimeUniqueIDsManager = new RuntimeUniqueIDsManager();

        public IEnumerable<PawnData> AllPawnData => pawnData.Values
            .Where(value => value != null);

        public RV2Component(Game game)
        {
            // assign component to data utility for faster referencing
            RV2Mod.RV2Component = this;
            Log.Message($"Initialized RV2Component: {this}");
        }

        public override void FinalizeInit()
        {
            base.FinalizeInit();

            DebugTools.Reset.ResyncAllHediffs();
            GlobalVoreTrackerUtility.Initialize();
        }

        public PawnData GetPawnData(Pawn pawn)
        {
            PawnData data = pawnData.TryGetValue(pawn.thingIDNumber);
            if(data == null)
            {
                data = new PawnData(pawn);
                pawnData.Add(pawn.thingIDNumber, data);
            }
            return data;
        }

        public void HardReset()
        {
            pawnData.Clear();
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Deep.Look(ref RuntimeUniqueIDsManager, nameof(RuntimeUniqueIDsManager));
            Scribe_Collections.Look(ref pawnData, nameof(pawnData), LookMode.Value, LookMode.Deep);
            if(Scribe.mode == LoadSaveMode.LoadingVars)
            {
                if(RuntimeUniqueIDsManager == null)
                {
                    RuntimeUniqueIDsManager = new RuntimeUniqueIDsManager();
                }
                if(pawnData == null)
                {
                    pawnData = new Dictionary<int, PawnData>();
                }
            }
        }

        public void MigrateFromOldPawnData(Dictionary<int, PawnData> oldPawnData)
        {
            foreach(KeyValuePair<int, PawnData> oldPawnDataEntry in oldPawnData)
            {
                pawnData.SetOrAdd(oldPawnDataEntry.Key, oldPawnDataEntry.Value);
            }
            Log.Message($"Migrated {oldPawnData.Count} entries to new PawnData storage");
        }
    }
}
