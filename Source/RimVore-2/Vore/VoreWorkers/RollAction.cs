﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

using Verse;

namespace RimVore2
{
    public abstract class RollAction : IExposable
    {
        protected VoreRole target = VoreRole.Invalid;
        protected bool invert = false;
#pragma warning disable IDE0044 // Add readonly modifier
        private bool canBlockNextActions = false;
#pragma warning restore IDE0044 // Add readonly modifier
        public virtual bool CanBlockNextActions => canBlockNextActions;
        protected VoreTrackerRecord record;

        protected Pawn TargetPawn => record.GetPawnByRole(target);
        protected Pawn OtherPawn => record.GetPawnByRole(target) == PredatorPawn ? PreyPawn : PredatorPawn;
        protected Pawn PredatorPawn => record.Predator;
        protected Pawn PreyPawn => record.Prey;

        public RollAction() { }

        // return false if the action wants to block further actions
        public virtual bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            this.record = record;
            return true;
        }
        public virtual void ExposeData() { }
        public virtual IEnumerable<string> ConfigErrors()
        {
            yield break;
        }
    }

    public class RollAction_PlaySound : RollAction
    {
        SoundDef sound;

        public RollAction_PlaySound() : base()
        {
            target = VoreRole.Predator;
        }

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            if(!TargetPawn.Spawned || sound == null)
            {
                return false;
            }
            SoundManager.PlaySingleSound(TargetPawn, sound);
            return true;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(sound == null)
            {
                yield return "Required field \"sound\" not set";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref sound, "sound");
        }
    }

    public class RollAction_ShowMote : RollAction
    {
#if v1_2
        ThingDef mote;
#else
        FleckDef mote;
#endif

        public RollAction_ShowMote()
        {
            target = VoreRole.Predator;
        }

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            if(!TargetPawn.Spawned || mote == null)
            {
                return false;
            }
#if v1_2
            MoteMaker.ThrowMetaIcon(TargetPawn.Position, TargetPawn.Map, mote);
#else
            FleckMaker.ThrowMetaIcon(TargetPawn.Position, TargetPawn.Map, mote);
#endif
            return true;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(mote == null)
            {
                yield return "Required field \"mote\" not set";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref mote, "mote");
        }
    }

    public class RollAction_Digest : RollAction
    {
        public DamageDef damageDef;

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);

            AcidUtility.ApplyAcidByDigestionProgress(record, record.CurrentVoreStage.PercentageProgress, damageDef);
            return true;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(damageDef == null)
            {
                yield return "Required field \"damageDef\" must be set";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref damageDef, "damageDef");
        }
    }

    public abstract class RollAction_CreateThings : RollAction
    {
        protected abstract List<ThingDef> ThingDefsToCreate { get; }

        float requiredNutrition = 1f;
        int quantityPerRequiredNutrition = 1;
        bool reducePreyNutrition = true;

        float nutritionReserve = 0;

        public RollAction_CreateThings() : base() { }

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            float currentPreyNutrition = record.PassValues.TryGetValue("PreyNutrition", 0f);
            int producedThingCount;
            // reduce rollstrength if strength is higher than leftover nutrition
            rollStrength = Math.Min(currentPreyNutrition, rollStrength);
            nutritionReserve += rollStrength;
            if(requiredNutrition > 0f)
            {
                producedThingCount = (int)Math.Floor(nutritionReserve / requiredNutrition);
            }
            else
            {
                producedThingCount = 1;
            }
            if(RV2Log.ShouldLog(true, "OngoingVore"))
                RV2Log.Message($"Created {producedThingCount} items, current nutrition reserves: {nutritionReserve}", false, "OngoingVore");
            producedThingCount *= quantityPerRequiredNutrition;
            if(producedThingCount <= 0)
            {
                return false;
            }
            if(reducePreyNutrition)
            {
                nutritionReserve -= producedThingCount * requiredNutrition;
            }
            if(ThingDefsToCreate.NullOrEmpty())
            {
                RV2Log.Warning("Tried to create items, but list of items to create is empty", "OngoingVore");
                return false;
            }
            producedThingCount = Mathf.CeilToInt(producedThingCount / ThingDefsToCreate.Count());
            foreach(ThingDef thingDef in ThingDefsToCreate)
            {
                Thing createdThing = CreateThing(thingDef, producedThingCount);
                if(RV2Log.ShouldLog(true, "OngoingVore"))
                    RV2Log.Message($"Created {createdThing.def.label}, stackCount: {createdThing.stackCount}", false, "OngoingVore");
                record.VoreContainer.TryAddOrTransfer(createdThing);
            }
            return true;
        }

        protected Thing CreateThing(ThingDef thingDef, int stackCount)
        {
            Thing thing = ThingMaker.MakeThing(thingDef);
            thing.stackCount = stackCount;
            return thing;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(invert)
            {
                yield return "Cannot use \"invert\" for thing creation";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref requiredNutrition, "requiredNutrition");
            Scribe_Values.Look(ref nutritionReserve, "nutritionReserve");
            Scribe_Values.Look(ref quantityPerRequiredNutrition, "quantityPerRequiredNutrition");
            Scribe_Values.Look(ref reducePreyNutrition, "reducePreyNutrition");
        }
    }

    public class RollAction_CreateThingsFromDef : RollAction_CreateThings
    {
        List<ThingDef> things;
        protected override List<ThingDef> ThingDefsToCreate => things;

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(things.NullOrEmpty())
            {
                yield return "required list \"things\" is not set or empty";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Collections.Look(ref things, "things", LookMode.Def);
        }
    }

    public class RollAction_CreateThingsFromContainer : RollAction_CreateThings
    {
        protected override List<ThingDef> ThingDefsToCreate => record.VoreContainer?.VoreProductContainer?.ProvideItems();
    }

    public class RollAction_CreateThingsFromEggComp : RollAction_CreateThings
    {
        protected override List<ThingDef> ThingDefsToCreate
        {
            get
            {
                return new List<ThingDef>() { GetEggDef(record.Predator) };
            }
        }

        /// <summary>
        /// - no egg comp -> fallback
        /// - no fertilized egg in comp -> unfertilized egg 
        /// - no unfertilized egg in comp -> fallback
        /// </summary>
        private ThingDef GetEggDef(Pawn pawn)
        {
            CompEggLayer eggComp = record.Predator.TryGetComp<CompEggLayer>();
            ThingDef fallbackDef = ThingDef.Named("EggChickenUnfertilized");
            if(eggComp == null)
            {
                if(RV2Log.ShouldLog(true, "OngoingVore"))
                    RV2Log.Message($"No CompEggLayer, using fallback: {fallbackDef.defName}", false, "OngoingVore");
                return fallbackDef;
            }
            ThingDef eggDef = eggComp.Props.eggUnfertilizedDef;
            if(eggDef != null)
            {
                if(RV2Log.ShouldLog(true, "OngoingVore"))
                    RV2Log.Message("Found CompEggLayer with unfertilized egg def", false, "OngoingVore");
                return eggDef;
            }
            eggDef = eggComp.Props.eggFertilizedDef;
            if(eggDef != null)
            {
                if(RV2Log.ShouldLog(true, "OngoingVore"))
                    RV2Log.Message($"Found CompEggLayer with fertilized egg def", false, "OngoingVore");
                return eggDef;
            }
            if(RV2Log.ShouldLog(true, "OngoingVore"))
                RV2Log.Message($"Found CompEggLayer with neither egg def, using fallback: {fallbackDef.defName}", false, "OngoingVore");
            return fallbackDef;
        }
    }

    public class RollAction_IncreaseNeed : RollAction
    {
        public NeedDef need;
        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            if(invert)
            {
                rollStrength *= -1;
            }
            return RV2PawnUtility.TryIncreaseNeed(TargetPawn, need, rollStrength);
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(target == VoreRole.Invalid)
            {
                yield return "required field \"target\" is not set";
            }
            if(need == null)
            {
                yield return "required field \"need\" is not set";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref need, "need");
        }
    }

    public class RollAction_Heal : RollAction
    {
        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            rollStrength *= 10; // there used to be a completely new roll after leeching food from predator, but with the new setup and no boxing rolls, we need to manually change the rolled value (or implement more RollActions that affect the rollStrength)

            IEnumerable<Hediff> tendableInjuries = TargetPawn.health.hediffSet.GetHediffsTendable();
            if(!tendableInjuries.EnumerableNullOrEmpty())
            {
                return TendPawn(tendableInjuries, rollStrength);
            }
            else
            {
                return HealPawn(rollStrength);
            }
        }

        private bool TendPawn(IEnumerable<Hediff> injuries, float rollStrength)
        {
            float quality = rollStrength;
            Hediff injury = injuries.RandomElement();
#if v1_2
            injury.Tended(quality);
#else
            injury.Tended(quality, quality);
#endif
            if(RV2Log.ShouldLog(false, "OngoingVore"))
                RV2Log.Message($"Tended injury {injury.Label} with quality {rollStrength}", "OngoingVore");
            return true;
        }

        private bool HealPawn(float rollStrength)
        {
            float quality = rollStrength;
            List<Hediff> injuries = TargetPawn.GetHealableInjuries();

            if(injuries.NullOrEmpty())
            {
                if(RV2Log.ShouldLog(false, "OngoingVore"))
                    RV2Log.Message("No injuries to heal", "OngoingVore");
                return false;
            }
            Hediff_Injury injury = (Hediff_Injury)injuries.RandomElement();
            injury.Severity -= quality;
            if(RV2Log.ShouldLog(false, "OngoingVore"))
                RV2Log.Message("Healed " + TargetPawn.Label + "'s hediff " + injury.def.label + " by " + quality, "OngoingVore");
            TargetPawn.health.hediffSet.DirtyCache();
            return true;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(invert)
            {
                yield return "Cannot use \"invert\" for healing";
            }
        }
    }

    public class RollAction_AddHediff : RollAction
    {
        public HediffDef hediff;
        protected BodyPartDef partDef;

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);

            if(TargetPawn.health.hediffSet.HasHediff(hediff))
            {
                return false;
            }
            // no reason to null check, null body part is whole body and valid
            BodyPartRecord bodyPart = TargetPawn.GetBodyPartByDef(partDef);
            Hediff actualHediff = TargetPawn.health.AddHediff(hediff, bodyPart);
            if(RV2Log.ShouldLog(false, "OngoingVore"))
                RV2Log.Message($"Added hediff {actualHediff.def.defName} to {TargetPawn.Label}'s body part {bodyPart}", "OngoingVore");
            return true;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(hediff == null)
            {
                yield return "required field \"hediff\" is not set";
            }
            if(target == VoreRole.Invalid)
            {
                yield return "required field \"target\" is not set";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref hediff, "hediff");
            Scribe_Defs.Look(ref partDef, "partDef");
        }
    }

    public class RollAction_IncreaseHediffSeverity : RollAction_AddHediff
    {
        bool addHediffIfNotPresent = false;

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            this.record = record;
            if(invert)
            {
                rollStrength *= -1;
            }

            HediffDef hediffDefToUse = hediff;
            QuirkManager quirkManager = TargetPawn.QuirkManager(false);
            // if the pawn has quirks, there may be replacements for the hediff, we want to affect that hediff in this circumstance
            if(quirkManager != null)
            {
                if(quirkManager.TryGetOverriddenHediff(hediff, out HediffDef potentialOverrideHediff))    
                {
                    if(potentialOverrideHediff == null)
                    {
                        if(RV2Log.ShouldLog(false, "OngoingVore"))
                            RV2Log.Message($"{TargetPawn.LabelShort} has override with NULL for hediff {hediff.defName}, not increasing severity", "OngoingVore");
                        return false;
                    }

                    if(RV2Log.ShouldLog(false, "OngoingVore"))
                        RV2Log.Message($"Found override hediff {potentialOverrideHediff.defName} for {TargetPawn.LabelShort}'s {hediff.defName}, increasing its severity instead of original", "OngoingVore");
                    hediffDefToUse = potentialOverrideHediff;
                }
            }

            if(!TargetPawn.health.hediffSet.HasHediff(hediffDefToUse))
            {
                if(addHediffIfNotPresent)
                {
                    if(RV2Log.ShouldLog(false, "OngoingVore"))
                        RV2Log.Message($"Adding non-present hediff {hediffDefToUse} to {TargetPawn.LabelShort}", "OngoingVore");
                    // if the hediff add didn't work, don't try to increase severity
                    if(!base.TryAction(record, rollStrength))
                    {
                        if(RV2Log.ShouldLog(false, "OngoingVore"))
                            RV2Log.Message("Failed to add hediff, cancelling severity increase", "OngoingVore");
                        return false;
                    }
                }
                else
                    return false;
            }
            Hediff targetHediff = base.TargetPawn.health.hediffSet.hediffs
                .FindAll(hed => hed.def == hediffDefToUse)
                .RandomElement();
            if(targetHediff == null)
            {
                return false;
            }
            targetHediff.Severity += rollStrength;
            if(RV2Log.ShouldLog(false, "OngoingVore"))
                RV2Log.Message($"Increased {TargetPawn.Label}'s {targetHediff.def.label} by {rollStrength} to {targetHediff.Severity}", "OngoingVore");
            return true;
        }
    }

    public class RollAction_Reform : RollAction
    {
        public PawnKindDef resultingPawnKind;
        ThingDef resultingRace;
        bool usePredatorRace = true;
        float eachPredatorTraitCopyChance = 0.5f;
        float eachPreyTraitCopyChance = 0.5f;
        bool changeFactionToPlayer = false;
        bool changeFactionToPredator = true;
        Gender? fixedGender = null;

        Pawn Predator => record.Predator;
        ThingDef PredatorRace => Predator.def;

        Pawn ResultSourcePawn => usePredatorRace ? Predator : TargetPawn;

        PawnKindDef ResultPawnKindDef
        {
            get
            {
                if(resultingPawnKind != null)
                {
                    return resultingPawnKind;
                }
                return ResultSourcePawn.kindDef;
            }
        }
        ThingDef ResultRaceDef
        {
            get
            {
                if(resultingRace != null)
                {
                    return resultingRace;
                }
                return ResultSourcePawn.def;
            }
        }

        Gender ResultGender => fixedGender != null ? fixedGender.Value : TargetPawn.gender;

        public RollAction_Reform()
        {
            target = VoreRole.Prey;
        }

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            if(TargetPawn.Dead)
            {
                NotificationUtility.DoNotification(NotificationType.MessageNeutral, "RV2_Message_ReformationFailed_PawnDead".Translate());
                return false;
            }
            VoreTracker tracker = record.Predator.PawnData().VoreTracker;
            float currentProgress = record.CurrentVoreStage.PercentageProgress;
            if(currentProgress <= 0.95)
            {
                if(RV2Log.ShouldLog(false, "OngoingVore"))
                    RV2Log.Message($"Not reforming pawn because progress is < 95% ({currentProgress})", "OngoingVore");
                return false;
            }
            Pawn newPawn = MakeReformedPawn();
            if(newPawn == null)
            {
                return false;
            }
            VoreTrackerRecord newRecord = tracker.SplitOffNewVore(record, newPawn, null, record.VorePathIndex + 1);
            tracker.UntrackVore(record);
            FakeNewPawnRecords(newRecord);
            StripAndDestroyOriginalPawn(newRecord);
            //GenSpawn.Spawn(newPawn, Predator.Position, Predator.Map);
            return true;
        }

        private void StripAndDestroyOriginalPawn(VoreTrackerRecord record)
        {
            record.VoreContainer.TryAddOrTransfer(TargetPawn.apparel?.WornApparel);
            record.VoreContainer.TryAddOrTransfer(TargetPawn.equipment?.AllEquipmentListForReading);
            TargetPawn.Destroy();
        }

        private Pawn MakeReformedPawn()
        {
            if(ResultPawnKindDef == null)
            {
                RV2Log.Error("Can not reform prey " + TargetPawn?.Label + ", ResultPawnKind is NULL");
                return null;
            }
            string originalPawnKindRaceDefName = ResultPawnKindDef.race.defName; // remember the original race to re-set it after generation
            try
            {
                if(ResultRaceDef == null)
                {
                    RV2Log.Error("Can not reform prey " + TargetPawn?.Label + ", ResultRaceDef is NULL");
                    return null;
                }
                ResultPawnKindDef.race = ResultRaceDef;    // temporarily overwrites the global PawnKindDef's race! Will be forced to be reset after generation
                List<Trait> newTraits = MakeNewTraits();
                Faction newFaction;
                if(changeFactionToPlayer)
                {
                    newFaction = Faction.OfPlayer;
                }
                else if(changeFactionToPredator)
                {
                    newFaction = Predator.Faction;
                }
                else
                {
                    newFaction = TargetPawn.Faction;
                }
                string fixedLastName = null;
                if(Predator.Name is NameTriple tripleName)
                {
                    fixedLastName = tripleName.Last;
                }
                PawnGenerationRequest request = new PawnGenerationRequest(
                    kind: ResultPawnKindDef,
                    fixedGender: ResultGender,
                    faction: newFaction,
                    forceGenerateNewPawn: true,
                    allowDowned: true,
                    canGeneratePawnRelations: false,
                    colonistRelationChanceFactor: 0,
                    allowFood: false,
                    allowAddictions: false,
                    relationWithExtraPawnChanceFactor: 0,
                    forcedTraits: null, // can't be used to pass predator / old pawn traits, due to forcing all degree to be set to 0 (why, tynan)
                                        //fixedBiologicalAge: 0.01f,  // will be overwritten post-generation
                                        //fixedChronologicalAge: TargetPawn.ageTracker?.AgeChronologicalYearsFloat,
                    fixedLastName: fixedLastName
#if v1_2
#else   // forgive me for the weird off-line ',' but the build variable has priority
                , fixedIdeo: Predator.Ideo ?? base.TargetPawn.ideo?.Ideo // use predators ideo if available, otherwise own ideo or null ideo
#endif
                );
                // for non-humanoids we set the new pawn to be a baby, or if the user is fine with generating age 0 humanoids, we use that
                bool generateNewborn = !ResultSourcePawn.IsHumanoid()
                    || (ResultSourcePawn.IsHumanoid() && RV2Mod.Settings.fineTuning.ReformHumanoidsAsNewborn);
#if v1_3
                request.Newborn = generateNewborn;
#else
                if(generateNewborn)
                {
                    request.AllowedDevelopmentalStages = DevelopmentalStage.Newborn;
                }
                else
                {
                    request.AllowedDevelopmentalStages = DevelopmentalStage.Adult;
                }
#endif

                Pawn generatedPawn = PawnGenerator.GeneratePawn(request);
                // if the generated pawn is not a newborn, we need to calculate a biological age for them
                if(!generateNewborn)
                {
                    SetReformedBiologicalAge(generatedPawn);
                }

                // the chronological age we always try to set to the previous pawns age
                SetReformedChronologicalAge(generatedPawn);
                // get rid of any apparel the pawn generated with
                generatedPawn.apparel?.DestroyAll();
                ForceReformedName(generatedPawn);
                // base game is retarded. genuinely fucking stupid, the following line is what happens when you pass a forcedTraits via request
                // pawn.story.traits.GainTrait(new Trait(traitDef, 0, true));
                // it just forces degree 0 - which traits with ACTUAL degrees do not have, 
                // so we skip the INTENDED mechanic of forcing traits and do it after the pawn is generated
                // this solution is dogshit, because at this point the traits don't have an impact on other aspects of pawn generation
                RV2PawnUtility.SetTraits(generatedPawn, newTraits);
                ForceParent(generatedPawn);
                DestroyAllBelongings(generatedPawn);
                return generatedPawn;
            }
            finally
            {
                // this appears to be unnecessary, but not doing it would leave the pawnKind with an incorrect ThingDef reference as race
                ResultPawnKindDef.race = ThingDef.Named(originalPawnKindRaceDefName);
            }
        }

        /// <summary>
        /// artificially increase the new pawns records to make the current vore count
        /// </summary>
        private void FakeNewPawnRecords(VoreTrackerRecord record)
        {
            record.Prey.records?.Increment(RV2_Common.preyRecordDef);
            record.VoreType.IncrementRecords(record.Predator, record.Prey);
        }

        private void ForceParent(Pawn newPawn)
        {
            if(Predator.gender == Gender.Male)
            {
                newPawn.SetFather(Predator);
            }
            else if(Predator.gender == Gender.Female)
            {
                newPawn.SetMother(Predator);
            }
        }

        private void DestroyAllBelongings(Pawn pawn)
        {
            pawn.equipment?.DestroyAllEquipment();
            pawn.apparel?.DestroyAll();
            pawn.inventory?.DestroyAll();
        }

        private void SetReformedBiologicalAge(Pawn pawn)
        {
            // animals, mechs, all the other stuff is set to age 0, they should have just been tagged as newborn
            if(!pawn.IsHumanoid())
            {
                pawn.ageTracker.AgeBiologicalTicks = 0;
                return;
            }
            // but because humans are weird, we do a whole lot of calculations for a good target age
            //float oldAge = TargetPawn.ageTracker.AgeBiologicalYearsFloat;
            // first get the minimum age required for this pawn to participate in vore
            float targetAge = RV2Mod.Settings.rules.GetValidAge(pawn);
            //// if our target age is above the previous age, reduce it to the old age
            //if(targetAge > oldAge)
            //{
            //    targetAge = oldAge;
            //}
            // if our target age is above chronological age, reduce to chronological age
            float chronoAge = pawn.ageTracker.AgeChronologicalYearsFloat;
            if(targetAge > chronoAge)
            {
                targetAge = chronoAge;
            }
            long newAge = Math.Max(0, (long)(targetAge * 3600000L));
            pawn.ageTracker.AgeBiologicalTicks = newAge;
        }

        private void SetReformedChronologicalAge(Pawn pawn)
        {
            // the new generated age MIGHT be larger than the old pawns chronological age, but chrono age must always be higher than bio age
            float newAge = pawn.ageTracker.AgeBiologicalYearsFloat;
            float chronoAge = TargetPawn.ageTracker.AgeChronologicalYears;
            if(newAge > chronoAge)
            {
                chronoAge = newAge;
            }
            pawn.ageTracker.AgeChronologicalTicks = (long)(chronoAge * 3600000L);
        }

        private void ForceReformedName(Pawn pawn)
        {
            // Animals have no real name, so no name to take
            if(TargetPawn.Name == null)
            {
                return;
            }
            // new name is triple name
            if(pawn.Name is NameTriple newName)
            {
                // both pawns have triple names, keep first and nick name, adopt last name
                if(TargetPawn.Name is NameTriple oldName)
                {
                    pawn.Name = new NameTriple(
                        oldName.First,
                        oldName.Nick,
                        newName.Last
                    );
                }
                // only new name is triple, old is single, so set first name to old single name
                else
                {
                    NameSingle oldSingleName = (NameSingle)TargetPawn.Name;
                    pawn.Name = new NameTriple(
                        oldSingleName.Name,
                        newName.Nick,
                        newName.Last
                    );
                }
            }
            // new name is single name, we always want to keep the old name
            else
            {
                pawn.Name = TargetPawn.Name;
            }
        }

        public override IEnumerable<string> ConfigErrors()
        {
            if(!usePredatorRace && resultingRace == null)
            {
                yield return "when using \"usePredatorRace\" = false the field \"resultingRace\" must be set!";
            }
        }

        protected virtual List<Trait> MakeNewTraits(PawnGenerationRequest request = default(PawnGenerationRequest))
        {
            List<Trait> newTraits = new List<Trait>();
            newTraits.AddRange(GetTraitsFromPawn(TargetPawn, eachPreyTraitCopyChance));
            newTraits.AddRange(GetTraitsFromPawn(Predator, eachPredatorTraitCopyChance));
            if(RV2Log.ShouldLog(false, "TraitGeneration"))
                RV2Log.Message($"total new traits: {string.Join(", ", newTraits.ConvertAll(t => t.Label))}", "TraitGeneration");

            int maxTraitsAllowed = RV2Mod.Settings.cheats.ReformedPawnTraitCount;
            if(newTraits.Count > maxTraitsAllowed)
            {
                if(RV2Log.ShouldLog(false, "TraitGeneration"))
                    RV2Log.Message($"too many new traits, current count: {newTraits.Count} allowed: {maxTraitsAllowed}", "TraitGeneration");
                newTraits = newTraits.GetRange(0, maxTraitsAllowed - 1);
            }
            return newTraits;
        }

        protected virtual List<Trait> GetTraitsFromPawn(Pawn pawn, float chance)
        {
            List<Trait> traits;
            traits = pawn.story?.traits?.allTraits?
                .FindAll(t => Rand.Chance(chance));
            if(traits.NullOrEmpty())
            {
                return new List<Trait>();
            }
            if(RV2Log.ShouldLog(false, "TraitGeneration"))
                RV2Log.Message($"Pawn {pawn.Label} provides traits: {string.Join(", ", traits.ConvertAll(t => t.Label))}", "TraitGeneration");
            return traits;
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref resultingRace, "resultingRace");
            Scribe_Values.Look(ref usePredatorRace, "usePredatorRace");
            Scribe_Values.Look(ref eachPredatorTraitCopyChance, "eachPredatorTraitCopyChance");
            Scribe_Values.Look(ref eachPreyTraitCopyChance, "eachPreyTraitCopyChance");
            Scribe_Values.Look(ref changeFactionToPlayer, "changeFactionToPlayer");
        }
    }

    public abstract class RollAction_Steal : RollAction
    {
        protected VoreRole takeFrom = VoreRole.Invalid;
        protected VoreRole giveTo = VoreRole.Invalid;

        protected Pawn TakerPawn => record.GetPawnByRole(giveTo);
        protected Pawn GiverPawn => record.GetPawnByRole(takeFrom);

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(takeFrom == VoreRole.Invalid)
            {
                yield return "required field \"takeFrom\" must be set";
            }
            if(giveTo == VoreRole.Invalid)
            {
                yield return "required field \"giveTo\" must be set";
            }
            if(takeFrom != VoreRole.Invalid && takeFrom == giveTo) // no reason to present message if both giveTo and takeFrom are not set
            {
                yield return "can't use same role in fields \"takeFrom\" and \"giveTo\"";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref takeFrom, "takeFrom");
            Scribe_Values.Look(ref giveTo, "giveTo");
        }
    }

    public class RollAction_StealNeed : RollAction_Steal
    {
        public NeedDef need;

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            if(invert)
            {
                rollStrength *= -1;
            }
            if(rollStrength < 0)
            {
                RV2Log.Warning("rollStrength is negative for stealing need. This may lead to undesired effects", "OngoingVore");
            }
            bool takerCanTakeNeed = TakerPawn.needs?.TryGetNeed(need) != null    // taker has need
                && TakerPawn.needs.TryGetNeed(need).CurLevel + rollStrength < 1;    // and taker has capacity in the need to steal from giver
            if(!takerCanTakeNeed)
            {
                if(RV2Log.ShouldLog(true, "OngoingVore"))
                    RV2Log.Message($"No need to steal need {need.defName} - {TakerPawn.LabelShort} has no capacity for {rollStrength}", false, "OngoingVore");
                return false;
            }
            bool canStealFromGiver = RV2PawnUtility.TryIncreaseNeed(GiverPawn, need, -rollStrength);
            // if we successfully decreased the givers need, or we are allowed to increase needs without needing to decrease the givers need
            if(canStealFromGiver || RV2Mod.Settings.cheats.CanStealFromNullNeed)
            {
                return RV2PawnUtility.TryIncreaseNeed(TakerPawn, need, rollStrength);
            }
            return false;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(need == null)
            {
                yield return "required field \"need\" not set";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref need, "need");
        }
    }

    public abstract class RollAction_StealComparable<T> : RollAction_Steal
    {
        protected bool useFixed = true;
        protected bool useRandom = false;
        protected bool useTakersBest = false;
        protected bool useTakersWorst = false;
        protected bool useGiversBest = false;
        protected bool useGiversWorst = false;

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref useFixed, "useFixed");
            Scribe_Values.Look(ref useRandom, "useRandom");
            Scribe_Values.Look(ref useTakersBest, "useTakersBest");
            Scribe_Values.Look(ref useTakersWorst, "useTakersWorst");
            Scribe_Values.Look(ref useGiversBest, "useGiversBest");
            Scribe_Values.Look(ref useGiversWorst, "useGiversWorst");
        }

        public abstract IEnumerable<T> SelectionRetrieval(Pawn pawn);
        public abstract float ValueRetrieval(Pawn pawn, T obj);
        public abstract T FixedSelector { get; }

        public virtual T Choose()
        {
            if(useFixed)
            {
                return FixedSelector;
            }
            IEnumerable<T> giverSelection = SelectionRetrieval(GiverPawn);
            IEnumerable<T> takerSelection = SelectionRetrieval(TakerPawn);
            // intersect allows us to reduce the list to common objects for both pawns
            IEnumerable<T> selection = giverSelection.Intersect(takerSelection);
            if(selection.EnumerableNullOrEmpty())
            {
                return default(T);
            }
            if(RV2Log.ShouldLog(true, "OngoingVore"))
                RV2Log.Message($"Selection of {typeof(T)} available for {TakerPawn.LabelShort} & {GiverPawn.LabelShort}: {string.Join(", ", selection)}", false, "OngoingVore");
            // we can already decide if we have a random one to use
            if(useRandom)
            {
                selection.RandomElement();
            }
            // remove any objects that are only on one side
            giverSelection = giverSelection.Intersect(selection);
            takerSelection = takerSelection.Intersect(selection);
            if(RV2Log.ShouldLog(true, "OngoingVore"))
            {
                RV2Log.Message($"giver selection: {string.Join(", ", giverSelection.Select(s => s + ": " + ValueRetrieval(GiverPawn, s)))}", false, "OngoingVore");
                RV2Log.Message($"taker selection: {string.Join(", ", takerSelection.Select(s => s + ": " + ValueRetrieval(TakerPawn, s)))}", false, "OngoingVore");
            }
            if(useTakersBest)
            {
                return takerSelection.MaxBy(entry => ValueRetrieval(TakerPawn, entry));
            }
            if(useTakersWorst)
            {
                return takerSelection.MinBy(entry => ValueRetrieval(TakerPawn, entry));
            }
            if(useGiversBest)
            {
                return takerSelection.MaxBy(entry => ValueRetrieval(GiverPawn, entry));
            }
            if(useGiversWorst)
            {
                return takerSelection.MinBy(entry => ValueRetrieval(GiverPawn, entry));
            }
            RV2Log.Warning("Reached the end of Choose() logic, this should never happen");
            return default(T);
        }
    }

    /// One thing to keep in mind is that skills use exponentially more experience points per level, 
    ///   trying to steal someones level whilst at lvl19 will take way more giver skills to 
    ///   sum up the experience points necessary for the takers skill upgrade
    ///   
    public class RollAction_StealSkill : RollAction_StealComparable<SkillDef>
    {
        SkillDef skill;
        public override SkillDef FixedSelector => skill;

        public override IEnumerable<SkillDef> SelectionRetrieval(Pawn pawn)
        {
            IEnumerable<SkillDef> skills = pawn.skills?.skills?
                .Where(skill => !skill.TotallyDisabled)
                .Select(skill => skill.def);
            if(skills == null)
                return new List<SkillDef>();
            return skills;
        }

        public override float ValueRetrieval(Pawn pawn, SkillDef obj)
        {
            return pawn.skills.GetSkill(obj).Level;

        }

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            List<SkillRecord> takerSkills = TakerPawn?.skills?.skills;
            if(takerSkills.NullOrEmpty())
            {
                RV2Log.Warning("Can't steal skills for taker with no skills: " + TakerPawn?.Label);
                return false;
            }
            List<SkillRecord> giverSkills = GiverPawn?.skills?.skills;
            if(giverSkills.NullOrEmpty())
            {
                RV2Log.Warning("Can't steal skills from giver with no skills: " + GiverPawn?.Label);
                return false;
            }
            SkillDef skillDef = Choose();
            if(skillDef == null)
            {
                return false;
            }
            if(RV2Log.ShouldLog(true, "OngoingVore"))
                RV2Log.Message($"{TakerPawn.LabelShort} chose skillDef {skillDef.defName} to steal from {GiverPawn.LabelShort}", true, "OngoingVore");
            StealLevels(GiverPawn, TakerPawn, skillDef, rollStrength);
            return true;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(useFixed && skill == null)
            {
                yield return "Field \"skill\" is required if field \"useFixed\" is used";
            }
        }

        private void StealLevels(Pawn giver, Pawn taker, SkillDef targetSkill, float amount)
        {
            SkillRecord giverSkill = giver.skills.GetSkill(targetSkill);
            SkillRecord takerSkill = taker.skills.GetSkill(targetSkill);
            // okay, this is a bit complicated due to the exponential growth and base games horrendous tracking of XP, bear with me
            float stolenExperience = 0;

            // we start with stealing the in-progress experience
            float currentProgress = giverSkill.xpSinceLastLevel / giverSkill.XpRequiredForLevelUp;
            if(RV2Log.ShouldLog(true, "OngoingVore"))
                RV2Log.Message($"Stealing {amount} levels. {giver.LabelShort} current level progress: {currentProgress}, taking from that first", false, "OngoingVore");
            if(currentProgress <= 0)
            {
                // uuh, apparently this can happen, don't do anything I guess
            }
            else if(currentProgress > amount)
            {
                // simple case, we don't steal a full level, but "in progress" experience and that's it
                stolenExperience += amount * giverSkill.xpSinceLastLevel;
                giverSkill.xpSinceLastLevel -= stolenExperience;
                amount = 0;
            }
            else
            {
                // we take all XP in the currently progressing level
                amount -= currentProgress;
                stolenExperience += giverSkill.xpSinceLastLevel;
                giverSkill.xpSinceLastLevel = 0;
            }

            // now we take all the full levels
            int infiniteLoopPrevention = 20;    // I hate using while loops, these are mandatory in them in my opinion
            while(amount > 1 && infiniteLoopPrevention-- > 0)
            {
                // first we drop a full level
                giverSkill.levelInt--;
                amount--;
                // we can now use the required XP as the number of XP that we stole with the full level drop
                stolenExperience += giverSkill.XpRequiredForLevelUp;
            }

            // and finally wrap up the steal by emulating a new in-progress skill
            if(amount > 0)
            {
                // we need to drop ANOTHER level, and then add the remaining XP, dropping 0.2 means we drop a full level and fill XP by 80%
                giverSkill.levelInt--;
                giverSkill.xpSinceLastLevel = (1 - amount) * giverSkill.XpRequiredForLevelUp;
                stolenExperience += amount * giverSkill.XpRequiredForLevelUp;
                amount = 0;
            }

            if(RV2Log.ShouldLog(true, "OngoingVore"))
                RV2Log.Message($"Stole a total of {stolenExperience} experience", false, "OngoingVore");
            if(!RV2Mod.Settings.cheats.TraitStealIgnoresLearningFactor)
            {
                float learningFactor = takerSkill.LearnRateFactor(true);
                stolenExperience *= learningFactor;
                if(RV2Log.ShouldLog(true, "PostVore"))
                    RV2Log.Message($"Modified the stolen experience with learning factor of {learningFactor} for a new total experience of {stolenExperience}", false, "OngoingVore");
            }
            takerSkill.Learn(stolenExperience, true);
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref skill, "skill");
        }
    }

    public class RollAction_StealTrait : RollAction_Steal
    {
        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            List<Trait> takerTraits = TakerPawn.story?.traits?.allTraits;
            List<Trait> giverTraits = GiverPawn.story?.traits?.allTraits
                .Where(trait => !trait.def.HasModExtension<TraitExtension_NotStealableFlag>())
                .ToList();
            if(takerTraits == null || giverTraits.NullOrEmpty())    // taker just needs to be capable of having traits, giver must have at least one trait
            {
                if(RV2Log.ShouldLog(true, "PostVore"))
                    RV2Log.Message("Either participant has no traits, can't steal trait", false, "PostVore");
                // either pawn doesn't have traits, nothing to do
                return false;
            }
            if(RV2Log.ShouldLog(true, "PostVore"))
            {
                RV2Log.Message($"Giver traits: {string.Join(", ", giverTraits.Select(t => t.def.defName))}", false, "PostVore");
                RV2Log.Message($"Taker traits: {string.Join(", ", takerTraits.Select(t => t.def.defName))}", false, "PostVore");
            }
            if(takerTraits.Count >= RV2Mod.Settings.cheats.MaxPawnTraits)
            {
                if(RV2Log.ShouldLog(true, "PostVore"))
                    RV2Log.Message("Can't steal more traits, already at allowed maximum traits", false, "PostVore");
                return false;
            }
            // TODO: traits with degrees increased / decreased through stealing
            IEnumerable<Trait> potentialTraits = giverTraits
                .Where(giverTrait => takerTraits
                    .All(takerTrait => takerTrait.def != giverTrait.def)
                );
            if(potentialTraits.EnumerableNullOrEmpty())
            {
                return false;
            }
            Trait traitToSteal = potentialTraits.RandomElement();
            if(RV2Log.ShouldLog(true, "PostVore"))
                RV2Log.Message($"Stealing {traitToSteal.def.defName} trait from selection: {string.Join(", ", potentialTraits.Select(t => t.def.defName))}", false, "PostVore");
            // I think we can get away without re-instancing a new trait
#if v1_2
            Log.Warning("Can't remove traits for 1.2 - enjoy trait duplication, I can't justify fixing it for the 5 people that run this version - I suggest disabling whatever tried to steal traits entirely");
#else
            PreyPawn.story.traits.RemoveTrait(traitToSteal);
#endif
            PredatorPawn.story.traits.GainTrait(traitToSteal);
            return true;
        }
    }

    public abstract class RollAction_PassValue : RollAction
    {
        public string name;
        protected float minValue = float.MinValue;
        protected float maxValue = float.MaxValue;

        public override void ExposeData()
        {
            Scribe_Values.Look(ref name, "name");
            Scribe_Values.Look(ref minValue, "minValue");
            Scribe_Values.Look(ref maxValue, "maxValue");
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(name == null)
            {
                yield return "Required field \"name\" not provided";
            }
            if(minValue > maxValue)
            {
                yield return "field \"minValue\" is larger than field \"maxValue\"";
            }
        }
    }

    public class RollAction_PassValue_Set : RollAction_PassValue
    {
        private enum VorePassValueType
        {
            Value,
            PreyNutrition,
            DigestionProgress
        }
        protected float value = 0;

#pragma warning disable IDE0044 // Add readonly modifier
        private VorePassValueType type = VorePassValueType.Value;
#pragma warning restore IDE0044 // Add readonly modifier

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            switch(type)
            {
                case VorePassValueType.Value:
                    record.SetPassValue(name, value);
                    return true;
                case VorePassValueType.PreyNutrition:
                    float preyNutritionValue = VoreCalculationUtility.CalculatePreyNutrition(record.Prey, record.Predator);
                    record.SetPassValue(name, preyNutritionValue);
                    return true;
                case VorePassValueType.DigestionProgress:
                    float digestionProgressValue = DigestionUtility.GetPreviousDigestionProgress(record.Prey);
                    if(digestionProgressValue > 0 && RV2Log.ShouldLog(false, "OngoingVore"))
                    {
                        RV2Log.Message($"Resuming digestion progress at {digestionProgressValue}", "OngoingVore");
                    }
                    record.SetPassValue(name, digestionProgressValue);
                    return true;
                default:
                    RV2Log.Error("Unknown VorePassValue type: " + type, "OngoingVore");
                    return false;
            }
        }

        public float AbstractValue()
        {
            switch(type)
            {
                case VorePassValueType.Value:
                    return value;
                case VorePassValueType.PreyNutrition:
                    return 5.2f;   // the value that base game values have
                case VorePassValueType.DigestionProgress:
                    return 0;
                default:
                    return 0;
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref value, "value");
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(type != VorePassValueType.Value && value != 0)
            {
                yield return "WARNING: Field \"value\" is set when \"type\" is not \"Value\". The currently set type will overwrite whichever \"value\" you set, consider removing \"value\" or using \"type\"=\"Value\" instead.";
            }
        }
    }

    public class RollAction_PassValue_Remove : RollAction_PassValue
    {
        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            return record.PassValues.Remove(name);
        }
    }

    public class RollAction_PassValue_Add : RollAction_PassValue
    {
        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            float newValue = record.PassValues[name] + rollStrength;
            record.ModifyPassValue(name, newValue, minValue, maxValue);
            return true;
        }
    }

    public class RollAction_PassValue_Subtract : RollAction_PassValue
    {
        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            float newValue = record.PassValues[name] - rollStrength;
            record.ModifyPassValue(name, newValue, minValue, maxValue);
            return true;
        }
    }

    public class RollAction_RecordTale : RollAction
    {
#pragma warning disable CS0649 // assigned from DEF
        private TaleDef tale;
#pragma warning restore CS0649 // assigned from DEF
        protected virtual TaleDef Tale => tale;
        protected virtual Def AdditionalDef => null;

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);

            if(Tale == null)
            {
                return false;
            }
            List<object> parameters = new List<object>();
            if(record.Predator != null)
                parameters.Add(record.Predator);
            if(record.Prey != null)
                parameters.Add(record.Prey);
            if(AdditionalDef != null)
                parameters.Add(AdditionalDef);
            TaleRecorder.RecordTale(Tale, parameters.ToArray());
            return true;
        }
    }

    public class RollAction_RecordTale_VoreInitiation : RollAction_RecordTale
    {
        protected override TaleDef Tale => record.VorePath.def.initTale;
        protected override Def AdditionalDef => record.VorePath.def;
    }

    public class RollAction_RecordTale_GoalFinish : RollAction_RecordTale
    {
        protected override TaleDef Tale => record.VorePath.VoreGoal.goalFinishTale;
        protected override Def AdditionalDef => record.VorePath.VoreGoal;
    }

    public class RollAction_RecordTale_VoreExit : RollAction_RecordTale
    {
        protected override TaleDef Tale => record.VorePath.def.exitTale;
        protected override Def AdditionalDef => record.VorePath.def;
    }

    public class RollAction_IncrementRecord : RollAction
    {

#pragma warning disable CS0649 // assigned from DEF
        private RecordDef recordDef;
#pragma warning restore CS0649 // assigned from DEF

        protected virtual RecordDef RecordDef => recordDef;

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            if(TargetPawn.records == null)
            {
                return false;
            }
            TargetPawn.records.Increment(RecordDef);
            return true;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(target == VoreRole.Invalid)
            {
                yield return "required field \"target\" not set";
            }
            if(recordDef == null && (this.GetType() == typeof(RollAction_IncrementRecord))) // only check the recordDef field if we are using the base class
            {
                yield return "required field \"recordDef\" not set";
            }
        }
    }

    public class RollAction_IncreaseGenitalPartSize : RollAction
    {
        string genitalHediffAlias;
        float maxTotalSeverity = 1.5f;
        float minTotalSeverity = 0f;
        const float preyBodySizeToSeverityModifier = 0.1f;
        float additionalMultiplier = 1f;

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            Pawn predator = record.Predator;
            Pawn prey = record.Prey;
            Hediff genital = BodyPartUtility.GetHediffByAlias(predator, genitalHediffAlias);
            if(genital == null)
            {
                if(RV2Log.ShouldLog(false, "OngoingVore"))
                    RV2Log.Message("Tried to increase genital size, but no genitals found", "OngoingVore");
                return false;
            }
            float severityChange = prey.BodySize * preyBodySizeToSeverityModifier;
            severityChange *= additionalMultiplier;
            float newSeverity = genital.Severity + severityChange;
            newSeverity = newSeverity.LimitClamp(minTotalSeverity, maxTotalSeverity);
            if(RV2Log.ShouldLog(false, "OngoingVore"))
                RV2Log.Message($"Increasing genital size of {genital.def.defName} by {severityChange} new severity: {newSeverity}", "OngoingVore");
            genital.Severity = newSeverity;
            return true;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(genitalHediffAlias == null)
            {
                yield return "Required field \"genitalHediffAlias\" is not set";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref genitalHediffAlias, "genitalHediffAlias");
        }
    }

    public class RollAction_ApplyDamage : RollAction
    {
        DamageDef damageDef;
        float armorPenetration = 0f;
        float angle = -1f;
        // BodyPartRecord hitPart = null;  // can't really set this in XML, would need to use a matching logic based on BodyPartDef

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            DamageInfo dinfo = new DamageInfo(damageDef, rollStrength, armorPenetration, angle);
            DamageWorker.DamageResult result = TargetPawn.TakeDamage(dinfo);
            return result.totalDamageDealt > 0;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(target == VoreRole.Invalid)
            {
                yield return "required field \"target\" is not set";
            }
            if(damageDef == null)
            {
                yield return "required field \"damageDef\" is not set";
            }
        }
    }

    public class RollAction_GainPsyfocus : RollAction
    {
        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);

            if(!TargetPawn.HasPsylink)
            {
                return false;
            }
            TargetPawn.psychicEntropy.OffsetPsyfocusDirectly(rollStrength);

            if(RV2Log.ShouldLog(false, "OngoingVore"))
                RV2Log.Message($"Increasing psyfocus of {TargetPawn.LabelShort} by {rollStrength} new psyfocus: {TargetPawn.psychicEntropy.CurrentPsyfocus}", "OngoingVore");
            return true;
        }
    }

    /* I don't know about these... went with implementing them directly in the PreVore and PostVore hooks */
    //public class RollAction_IncrementRecord_Type : RollAction_IncrementRecord
    //{
    //    protected override RecordDef RecordDef
    //    {
    //        get
    //        {
    //            switch (target)
    //            {
    //                case VoreRole.Predator:
    //                    return record.VoreType.initiationRecordPredator;
    //                case VoreRole.Prey:
    //                    return record.VoreType.initiationRecordPrey;
    //                default:
    //                    return null;
    //            }
    //        }
    //    }
    //}

    //public class RollAction_IncrementRecord_Goal : RollAction_IncrementRecord
    //{
    //    protected override RecordDef RecordDef
    //    {
    //        get
    //        {
    //            switch (target)
    //            {
    //                case VoreRole.Predator:
    //                    return record.VoreGoal.goalFinishRecordPredator;
    //                case VoreRole.Prey:
    //                    return record.VoreGoal.goalFinishRecordPrey;
    //                default:
    //                    return null;
    //            }
    //        }
    //    }
    //}
}