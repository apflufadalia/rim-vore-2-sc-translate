﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RimVore2
{
    public static class RV2ComponentUtility
    {
        public static PawnData PawnData(this Pawn pawn)
        {
            if(pawn == null)
            {
                RV2Log.Error("Tried to get pawn data for NULL pawn!");
                return null;
            }
            return RV2Mod.RV2Component.GetPawnData(pawn);
        }
    }
}
