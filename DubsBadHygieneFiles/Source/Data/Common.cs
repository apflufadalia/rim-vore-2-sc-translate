﻿using RimVore2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RV2_DBH
{
    public static class Common
    {
        public static bool ValidForToiletDisposal(VoreTrackerRecord record)
        {
            return record.VoreContainer?.VoreProductContainer != null
                && !record.VoreGoal.HasModExtension<InvalidForToiletDisposalFlag>()
                && record.HasReachedEnd;
        }
    }
}
